package com.androidrecipes.javascript;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
//Crea una activity configurada anteriormente con código HTML
@TargetApi(19)
public class MyActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView webview = new WebView(this);
        //JAvaScript habilitad apor defecto
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(mClient);
        //Interfaz personalizada
        webview.addJavascriptInterface(new MyJavaScriptInterface(), "BRIDGE");

        setContentView(webview);

        //webview.loadUrl("file:///android_asset/form.html");
        webview.loadUrl("file:///android_asset/MyFormulario.html");
    }

    private static final String JS_SETELEMENT =
            "javascript:document.getElementById('%s').value='%s'";
    private static final String JS_GETELEMENT =
            "javascript:window.BRIDGE.storeElement('%s',document.getElementById('%s').value)";
    private static final String ELEMENTID_1 = "emailAddress";
    private static final String ELEMENTID_2 = "nombres";
    private static final String ELEMENTID_3 = "direccions";
    private static final String ELEMENTID_4 = "artes";
    private static final String ELEMENTID_5 = "televisions";
    private static final String ELEMENTID_6 = "videojuegos";
    private static final String ELEMENTID_7 = "deportes";

    private WebViewClient mClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //recuper los datos usando JavaScrpit
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_1, ELEMENTID_1));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_2, ELEMENTID_2));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_3, ELEMENTID_3));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_4, ELEMENTID_4));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_5, ELEMENTID_5));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_6, ELEMENTID_6));
            executeJavascript(view,
                    String.format(JS_GETELEMENT, ELEMENTID_7, ELEMENTID_7));
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //Carga los elemntos de la pagina
            SharedPreferences prefs = getPreferences(Activity.MODE_PRIVATE);
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_1, ELEMENTID_1, prefs.getString(ELEMENTID_1, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_2, ELEMENTID_2, prefs.getString(ELEMENTID_2, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_3, ELEMENTID_3, prefs.getString(ELEMENTID_3, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_4, ELEMENTID_4, prefs.getString(ELEMENTID_5, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_5, ELEMENTID_5, prefs.getString(ELEMENTID_5, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_6, ELEMENTID_6, prefs.getString(ELEMENTID_6, "")));
            executeJavascript(view,
                    String.format(JS_SETELEMENT, ELEMENTID_7, ELEMENTID_7, prefs.getString(ELEMENTID_7, "")));
        }
    };

    private void executeJavascript(WebView view, String script) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            view.evaluateJavascript(script, null);
        } else {
            view.loadUrl(script);
        }
    }

    private class MyJavaScriptInterface {
        // Almacenar un elemento en preferencias
        @JavascriptInterface
        public void storeElement(String id, String element) {
            SharedPreferences.Editor edit = getPreferences(Activity.MODE_PRIVATE).edit();
            edit.putString(id, element);
            edit.commit();
            if (!TextUtils.isEmpty(element)) {
                Toast.makeText(MyActivity.this, element, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
