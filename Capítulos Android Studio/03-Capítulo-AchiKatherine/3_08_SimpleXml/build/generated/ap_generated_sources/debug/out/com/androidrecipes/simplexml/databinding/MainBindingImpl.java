package com.androidrecipes.simplexml.databinding;
import com.androidrecipes.simplexml.R;
import com.androidrecipes.simplexml.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class MainBindingImpl extends MainBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final android.widget.TextView mboundView6;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public MainBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private MainBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[4]
            , (android.widget.LinearLayout) bindings[0]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.address.setTag(null);
        this.amount.setTag(null);
        this.constraintLayout.setTag(null);
        this.mboundView6 = (android.widget.TextView) bindings[6];
        this.mboundView6.setTag(null);
        this.productName.setTag(null);
        this.timestamp.setTag(null);
        this.units.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.purchase == variableId) {
            setPurchase((com.androidrecipes.simplexml.Purchase) variable);
        }
        else if (BR.rawXml == variableId) {
            setRawXml((java.lang.String) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setPurchase(@Nullable com.androidrecipes.simplexml.Purchase Purchase) {
        this.mPurchase = Purchase;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.purchase);
        super.requestRebind();
    }
    public void setRawXml(@Nullable java.lang.String RawXml) {
        this.mRawXml = RawXml;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.rawXml);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer purchaseUnits = null;
        java.lang.Long purchaseAmount = null;
        java.lang.String purchaseCurrencyToStringPurchaseAmount = null;
        com.androidrecipes.simplexml.Purchase.Currency purchaseCurrency = null;
        java.util.Date purchaseTimestamp = null;
        java.lang.String stringValueOfPurchaseUnits = null;
        int androidDatabindingViewDataBindingSafeUnboxPurchaseUnits = 0;
        com.androidrecipes.simplexml.Purchase purchase = mPurchase;
        com.androidrecipes.simplexml.Purchase.Address purchaseDeliveryAddress = null;
        java.lang.String purchaseDeliveryAddressToString = null;
        java.lang.String purchaseProductName = null;
        java.lang.String rawXml = mRawXml;

        if ((dirtyFlags & 0x5L) != 0) {



                if (purchase != null) {
                    // read purchase.units
                    purchaseUnits = purchase.units;
                    // read purchase.amount
                    purchaseAmount = purchase.amount;
                    // read purchase.currency
                    purchaseCurrency = purchase.currency;
                    // read purchase.timestamp
                    purchaseTimestamp = purchase.timestamp;
                    // read purchase.deliveryAddress
                    purchaseDeliveryAddress = purchase.deliveryAddress;
                    // read purchase.productName
                    purchaseProductName = purchase.productName;
                }


                // read android.databinding.ViewDataBinding.safeUnbox(purchase.units)
                androidDatabindingViewDataBindingSafeUnboxPurchaseUnits = android.databinding.ViewDataBinding.safeUnbox(purchaseUnits);
                if (purchaseCurrency != null) {
                    // read purchase.currency.toString(purchase.amount)
                    purchaseCurrencyToStringPurchaseAmount = purchaseCurrency.toString(purchaseAmount);
                }
                if (purchaseDeliveryAddress != null) {
                    // read purchase.deliveryAddress.toString()
                    purchaseDeliveryAddressToString = purchaseDeliveryAddress.toString();
                }


                // read String.valueOf(android.databinding.ViewDataBinding.safeUnbox(purchase.units))
                stringValueOfPurchaseUnits = java.lang.String.valueOf(androidDatabindingViewDataBindingSafeUnboxPurchaseUnits);
        }
        if ((dirtyFlags & 0x6L) != 0) {
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.address, purchaseDeliveryAddressToString);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.amount, purchaseCurrencyToStringPurchaseAmount);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.productName, purchaseProductName);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.timestamp, com.androidrecipes.simplexml.SimpleXmlActivity.convertFromDate(purchaseTimestamp));
            android.databinding.adapters.TextViewBindingAdapter.setText(this.units, stringValueOfPurchaseUnits);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, rawXml);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): purchase
        flag 1 (0x2L): rawXml
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}