package com.androidrecipes.webview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
//Desde nuestra activity se llama a un pagina web
public class MyActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        double a=-0.254921;
        double b=-78.532306;
        double c=-0.198206;
        double d=-78.503516;
// se declaa un objeto WebView
        WebView webview = new WebView(this);
        //Enable JavaScript support
        webview.getSettings().setJavaScriptEnabled(true);
        // Colocamos el URL
        webview.loadUrl("https://www.google.com/maps/dir/?api=1&origin="+a+","+b+"&destination="+c+","+d);

        setContentView(webview);
    }
}

