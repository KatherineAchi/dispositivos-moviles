package com.androidrecipes.sms;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
//App que permite enviar mensajes
public class MainActivity extends SmsActivity {
    private EditText num1;
    private TextView texto;
    private Button boton;

// Llamamos a todos las clases creadas
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1=(EditText)findViewById(R.id.num1);

        boton=(Button)findViewById(R.id.boton);
    }

}

