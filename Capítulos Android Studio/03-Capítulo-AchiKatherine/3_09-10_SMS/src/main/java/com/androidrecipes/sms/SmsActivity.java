package com.androidrecipes.sms;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

@TargetApi(19)
public class SmsActivity extends Activity {
    //el numero a quiere mandar el mensaje
    private EditText num1;
    private TextView texto;

    //private static final String RECIPIENT_ADDRESS = "<+593 998821445>";

    //Entrega de mensaje
    private static final String ACTION_SENT =
            "com.examples.sms.SENT";
    private static final String ACTION_DELIVERED =
            "com.examples.sms.DELIVERED";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        num1=(EditText)findViewById(R.id.num1);
        texto=(TextView)findViewById(R.id.texto);
//Texto predefinido del mensaje
        Button sendButton = new Button(this);
        sendButton.setText("Katherine");
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS("Beam us up!");
            }
        });

        setContentView(sendButton);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Monitorea el estado
        registerReceiver(sent, new IntentFilter(ACTION_SENT));
        registerReceiver(delivered, new IntentFilter(ACTION_DELIVERED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Make sure receivers aren't active while we are in the background
        unregisterReceiver(sent);
        unregisterReceiver(delivered);
    }

    private void sendSMS(String message) {
        //Mensajes enviados
        PendingIntent sIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_SENT), 0);
        //Confirmación de entrega de mensajes
        PendingIntent dIntent = PendingIntent.getBroadcast(
                this, 0, new Intent(ACTION_DELIVERED), 0);

        //   Enviar el mensaje
        SmsManager manager = SmsManager.getDefault();
        manager.sendTextMessage(String.valueOf(num1), null, message,
                sIntent, dIntent);
    }

    //Registra los eventos
    private BroadcastReceiver sent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Enviado exitosamente
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                case SmsManager.RESULT_ERROR_NULL_PDU:
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    //Maneja errores
                    break;
            }
        }
    };

    /*
     * BroadcastReceiver that is registered to receive events when
     * an SMS delivery confirmation is received; with the result code.
     */
    private BroadcastReceiver delivered = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    //Handle delivery success
                    break;
                case Activity.RESULT_CANCELED:
                    //Handle delivery failure
                    break;
            }
        }
    };
}

