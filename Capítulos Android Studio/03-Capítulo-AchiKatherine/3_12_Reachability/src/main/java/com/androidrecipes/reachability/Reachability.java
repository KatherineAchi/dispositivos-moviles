package com.androidrecipes.reachability;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
//Maneja la conectividad del dispositivo
public class Reachability extends Activity {

    ReachabilityManager mReach;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
// Llama a ejecución a la nueva clase
        mReach = ReachabilityManager.getInstance(this);
        Toast.makeText(this, "Network " + mReach.isNetworkReachable() + "\nGoogle " + mReach.isHostReachable(0xD155E368), Toast.LENGTH_SHORT).show();
    }
}