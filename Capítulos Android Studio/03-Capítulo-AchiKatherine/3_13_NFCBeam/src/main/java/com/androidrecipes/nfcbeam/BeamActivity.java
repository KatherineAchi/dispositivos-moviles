package com.androidrecipes.nfcbeam;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateBeamUrisCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class BeamActivity extends Activity implements CreateBeamUrisCallback, OnNdefPushCompleteCallback {
    //Declara las variables
    private static final String TAG = "NfcBeam";
    private static final int PICK_IMAGE = 100;

    private NfcAdapter mNfcAdapter;
    private Uri mSelectedImage;

    private TextView mUriName;
    private ImageView mPreviewImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mUriName = (TextView) findViewById(R.id.text_uri);
        mPreviewImage = (ImageView) findViewById(R.id.image_preview);

        // Verifica el adaptador NFC disponible
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            mUriName.setText("NFC is not available on this device.");
        } else {
            // Registre la devolución de llamada para configurar el mensaje NDEF
            mNfcAdapter.setBeamPushUrisCallback(this, this);
            // Registre la devolución de llamada para escuchar el mensaje enviado correctamente
            mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            mUriName.setText(data.getData().toString());
            mSelectedImage = data.getData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //Verifica que la actividad haya comenzado debido a un Android Beam
        if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        // Maneja la intención
        setIntent(intent);
    }

    void processIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            mPreviewImage.setImageURI(data);
        } else {
            mUriName.setText("Received Invalid Image Uri");
        }
    }
//Método del click para cargar una imagen
    public void onSelectClick(View v) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);
    }
//Retorna la imagen seleccionada
    @Override
    public Uri[] createBeamUris(NfcEvent event) {
        if (mSelectedImage == null) {
            return null;
        }
        return new Uri[]{mSelectedImage};
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        Log.i(TAG, "Push Complete!");
    }
}
