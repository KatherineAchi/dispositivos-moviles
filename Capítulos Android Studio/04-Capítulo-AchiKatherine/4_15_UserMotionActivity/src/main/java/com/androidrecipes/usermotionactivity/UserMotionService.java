package com.androidrecipes.usermotionactivity;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
//Sensor que reconoce los comportaminentos del usuario del dispositivo
public class UserMotionService extends IntentService {
    private static final String TAG = "UserMotionService";

    public interface OnActivityChangedListener {
        public void onUserActivityChanged(int bestChoice, int bestConfidence,
                                          ActivityRecognitionResult newActivity);
    }

    //Última actividad detectada
    private DetectedActivity mLastKnownActivity;

    //Devolución de llamada
    private CallbackHandler mHandler;

    private static class CallbackHandler extends Handler {
        //Detecta los cambios
        private OnActivityChangedListener mCallback;

        public void setCallback(OnActivityChangedListener callback) {
            mCallback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            if (mCallback != null) {
                //Lee los datos y los devuelve
                ActivityRecognitionResult newActivity = (ActivityRecognitionResult) msg.obj;
                mCallback.onUserActivityChanged(msg.arg1, msg.arg2, newActivity);
            }
        }
    }

    public UserMotionService() {
        super("UserMotionService");
        mHandler = new CallbackHandler();
    }

    public void setOnActivityChangedListener(OnActivityChangedListener listener) {
        mHandler.setCallback(listener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "Service is stopping...");
    }

    //Procesamiento largo
    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            //resultado de la intención
            ActivityRecognitionResult result =
                    ActivityRecognitionResult.extractResult(intent);
            DetectedActivity activity = result.getMostProbableActivity();
            Log.v(TAG, "New User Activity Event");

            //Selecciona un lugar
            if (activity.getType() == DetectedActivity.UNKNOWN
                    && activity.getConfidence() < 60
                    && result.getProbableActivities().size() > 1) {
                //Selecciona el siguiente elemento probable
                activity = result.getProbableActivities().get(1);
            }

            //Avisa a la devolución de llamada
            if (mLastKnownActivity == null
                    || mLastKnownActivity.getType() != activity.getType()
                    || mLastKnownActivity.getConfidence() != activity.getConfidence()) {
                //pasa los resultados
                Message msg = Message.obtain(null,
                        0,                         //what
                        activity.getType(),        //arg1
                        activity.getConfidence(),  //arg2
                        result);
                mHandler.sendMessage(msg);
            }
            mLastKnownActivity = activity;
        }
    }

    //Vincula la activity con el servicio
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //Contenedor simple permite acceso directo
    private LocalBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public UserMotionService getService() {
            return UserMotionService.this;
        }
    }

    //se obtiene un nombre para cada estado
    public static String getActivityName(DetectedActivity activity) {
        switch (activity.getType()) {
            case DetectedActivity.IN_VEHICLE:
                return "Driving";
            case DetectedActivity.ON_BICYCLE:
                return "Biking";
            case DetectedActivity.ON_FOOT:
                return "Walking";
            case DetectedActivity.STILL:
                return "Not Moving";
            case DetectedActivity.TILTING:
                return "Tilting";
            case DetectedActivity.UNKNOWN:
            default:
                return "No Clue";
        }
    }
}
