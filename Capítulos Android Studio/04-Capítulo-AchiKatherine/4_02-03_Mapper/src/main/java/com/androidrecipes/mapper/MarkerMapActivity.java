package com.androidrecipes.mapper;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
//Muestra una o mas ubicaciones en el mapa, incluyendo la del usuario
public class MarkerMapActivity extends FragmentActivity implements
        RadioGroup.OnCheckedChangeListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.InfoWindowAdapter, OnMapReadyCallback {
    //Crea las variables y las declara
    private static final String TAG = "AndroidRecipes";
    private static final int REQUEST_CODE_PERMISSIONS = 10;

    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //Verifica que los servicios de juego estén activos y actualizados
        int resultCode = GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(this);
        switch (resultCode) {
            case ConnectionResult.SUCCESS:
                Log.d(TAG, "Google Play Services is ready to go!");
                break;
            default:
                showPlayServicesError(resultCode);
                return;
        }

        mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        // Conecta la interfaz del usuario tipo mapa
        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(false);
        typeSelect.setOnCheckedChangeListener(this);
        typeSelect.check(R.id.type_normal);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapFragment.getMapAsync(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMap = null;
    }
//Método que llama al mapa
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.type_satellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.type_normal:
            default:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
        }
    }
//Método que funciona al clickear

    @Override
    public boolean onMarkerClick(Marker marker) {
        // Activa o desactiva
        return false;
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        // Do something while the marker is moving
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        Log.i("MarkerTest", "Drag " + marker.getTitle()
                + " to " + marker.getPosition());
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d("MarkerTest", "Drag " + marker.getTitle()
                + " from " + marker.getPosition());
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Actua sobre el evento de selección
        marker.hideInfoWindow();
    }

//Método de información
    //Coloca el contenido en una ventana emergente
    @Override
    public View getInfoContents(Marker marker) {
        //Crea un marcador
        return null;
    }

//Muestra una ventana de información
    @Override
    public View getInfoWindow(Marker marker) {
        View content = createInfoView(marker);
        content.setBackgroundResource(R.drawable.background);
        return content;
    }

//Métodode ayuda privada
    private View createInfoView(Marker marker) {
        View content = getLayoutInflater().inflate(
                R.layout.info_window, null);
        ImageView image = (ImageView) content
                .findViewById(R.id.image);
        TextView text = (TextView) content
                .findViewById(R.id.text);

        image.setImageResource(R.drawable.ic_launcher);
        text.setText(marker.getTitle());

        return content;
    }

//Brinda ayuda al usuario para que actualice su versión
    private void showPlayServicesError(int errorCode) {
        // Obtener el cuadro de diálogo de error de los servicios de Google Play
        GoogleApiAvailability.getInstance()
                .showErrorDialogFragment(this, errorCode, 1,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                finish();
                            }
                        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Interacción con los elementos
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(this);

        // Google HQ 37.427,-122.099
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4218, -122.0840))
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.logo))
                .alpha(0.6f));
        marker.setDraggable(true);

        // Dibuja el mapa
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4118, -122.0740))
                .title("Neighbor #1")
                .snippet("Best Restaurant in Town")
                // Show a default marker, in the default color
                .icon(BitmapDescriptorFactory.defaultMarker()));

           mMap.addMarker(new MarkerOptions()
                .position(new LatLng(37.4318, -122.0940))
                .title("Neighbor #2")
                .snippet("Worst Restaurant in Town")

                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));

        // Permite el zoom
        LatLng mapCenter = new LatLng(37.4218, -122.0840);
        CameraUpdate newCamera = CameraUpdateFactory
                .newLatLngZoom(mapCenter, 13);
        mMap.moveCamera(newCamera);

        RadioGroup typeSelect = (RadioGroup) findViewById(R.id.group_maptype);
        typeSelect.setEnabled(true);
    }
}
