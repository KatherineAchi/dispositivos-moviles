package com.androidrecipes.videooverlay;

import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
//Grabación de video
public class VideoCaptureActivity extends Activity implements SurfaceHolder.Callback {
//Declara variables
    private Camera mCamera;
    private MediaRecorder mRecorder;

    private SurfaceView mPreview;
    private Button mRecordButton;

    private boolean mRecording = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
//Inicializa variables
        mRecordButton = (Button) findViewById(R.id.button_record);
        mRecordButton.setText("Start Recording");

        mPreview = (SurfaceView) findViewById(R.id.surface_video);
        mPreview.getHolder().addCallback(this);

        mCamera = Camera.open();
        //Gira la pantalla
        mCamera.setDisplayOrientation(90);
        mRecorder = new MediaRecorder();
    }

    @Override
    protected void onDestroy() {
        mCamera.release();
        mCamera = null;
        super.onDestroy();
    }

    public void onRecordClick(View v) {
        updateRecordingState();
    }

    //Inicializa la cámara y el video
    private void initializeRecorder() throws IllegalStateException, IOException {
        //Desbloquea la cámara
        mCamera.unlock();
        mRecorder.setCamera(mCamera);
        //Actualiza la cámara
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        //Configuración de salida
        File recordOutput = new File(Environment.getExternalStorageDirectory(), "recorded_video.mp4");
        if (recordOutput.exists()) {
            recordOutput.delete();
        }
        CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        mRecorder.setProfile(cpHigh);
        mRecorder.setOutputFile(recordOutput.getAbsolutePath());
        mRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());

        //Establece tiempo de duración
        mRecorder.setMaxDuration(50000); // 50 seconds
        mRecorder.setMaxFileSize(5000000); // Approximately 5 megabytes

        mRecorder.prepare();
    }

    private void updateRecordingState() {
        if (mRecording) {
            mRecording = false;
            //Restablece el estado dela grabación
            mRecorder.stop();
            mRecorder.reset();
            //Vista previa de la grabación
            mCamera.lock();
            mRecordButton.setText("Start Recording");
        } else {
            try {
                //Reinicia la grabadora
                initializeRecorder();
                //Inicia la grabación
                mRecording = true;
                mRecorder.start();
                mRecordButton.setText("Stop Recording");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //Vista previa de la cámara
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }
}
