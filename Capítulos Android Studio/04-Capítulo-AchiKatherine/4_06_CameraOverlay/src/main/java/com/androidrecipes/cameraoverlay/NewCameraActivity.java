package com.androidrecipes.cameraoverlay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//App con acceso directo a la cámara
public class NewCameraActivity extends AppCompatActivity {
//Llama a las clases creadas
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_camera);
    }
}
