package com.androidrecipes.logger;

import android.app.Activity;
import android.os.Bundle;

public class LoggerActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Esta declaración solo se imprimió en depuración
        Logger.d("Activity Created");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Esta declaración solo se imprimió en depuración
        Logger.d("Activity Resume at %d", System.currentTimeMillis());
        //Esta declaración siempre impresa
        Logger.i("It is now %d", System.currentTimeMillis());
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Esta declaración solo se imprimió en depuración
        Logger.d("Activity Pause at %d", System.currentTimeMillis());
        //Esto siempre impreso
        Logger.w("No, don't leave!");
    }
}
