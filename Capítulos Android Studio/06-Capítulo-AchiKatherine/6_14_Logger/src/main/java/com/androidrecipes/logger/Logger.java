package com.androidrecipes.logger;

import android.util.Log;

public class Logger {
    private static final String LOGTAG = "AndroidRecipes";

    private static String getLogString(String format, Object... args) {
        //Optimización menor, solo llame a String.format si es necesario
        if (args.length == 0) {
            return format;
        }

        return String.format(format, args);
    }

    //Los niveles de registro INFO, WARNING, ERROR se imprimen siempre

    public static void e(String format, Object... args) {
        Log.e(LOGTAG, getLogString(format, args));
    }

    public static void w(String format, Object... args) {
        Log.w(LOGTAG, getLogString(format, args));
    }

    public static void w(Throwable throwable) {
        Log.w(LOGTAG, throwable);
    }

    public static void i(String format, Object... args) {
        Log.i(LOGTAG, getLogString(format, args));
    }

    //Los niveles de registro DEBUG y VERBOSE están protegidos por el indicador DEBUG

    public static void d(String format, Object... args) {
        if (!BuildConfig.DEBUG) return;

        Log.d(LOGTAG, getLogString(format, args));
    }

    public static void v(String format, Object... args) {
        if (!BuildConfig.DEBUG) return;

        Log.v(LOGTAG, getLogString(format, args));
    }
}
