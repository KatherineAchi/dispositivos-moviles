package com.androidrecipes.appwidget;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
//Su aplicación proporciona información a la que los usuarios
// necesitan acceder de forma rápida y coherente.
// Quieres agregar un componente interactivo de su aplicación
// a la pantalla de inicio del usuario.
public class MainActivity extends Activity {

    private TextView mCurrentNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mCurrentNumber = (TextView) findViewById(R.id.text_number);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNumberView();
        //Registrar un receptor para recibir actualizaciones cuando finalice el servicio
        IntentFilter filter = new IntentFilter(RandomService.ACTION_RANDOM_NUMBER);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Anular el registro de nuestro receptor
        unregisterReceiver(mReceiver);
    }

    public void onRandomClick(View v) {
        //Llame al servicio para actualizar los datos del número
        startService(new Intent(this, RandomService.class));
    }

    private void updateNumberView() {
        //Actualizar la vista con el último número
        mCurrentNumber.setText(String.valueOf(RandomService.getRandomNumber()));
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Actualizar la vista con el nuevo número
            updateNumberView();
        }
    };
}
