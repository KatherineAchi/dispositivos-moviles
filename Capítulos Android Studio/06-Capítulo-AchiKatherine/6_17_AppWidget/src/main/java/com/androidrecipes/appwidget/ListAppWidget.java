package com.androidrecipes.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;

public class ListAppWidget extends AppWidgetProvider {

    //Este método se llama para actualizar los widgets creados por este proveedor
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Cuando caduca el updatePeriodMillis definido en AppWidgetProviderInfo
        for (int i = 0; i < appWidgetIds.length; i++) {
            Intent intent = new Intent(context, ListWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.list_widget_layout);
            //Establecer la vista de título según la configuración del widget
            SharedPreferences prefs = context.getSharedPreferences(String.valueOf(appWidgetIds[i]), Context.MODE_PRIVATE);
            String mode = prefs.getString(ListWidgetService.KEY_MODE, ListWidgetService.MODE_IMAGE);
            if (ListWidgetService.MODE_VIDEO.equals(mode)) {
                views.setTextViewText(R.id.text_title, "Video Collection");
            } else {
                views.setTextViewText(R.id.text_title, "Image Collection");
            }

            //Conecte el adaptador para completar los datos de la lista en
            //la forma de un Intent que apunta a nuestro RemoveViewsService
            views.setRemoteAdapter(appWidgetIds[i], R.id.list, intent);

            //Establecer la vista vacía para la lista
            views.setEmptyView(R.id.list, R.id.list_empty);

            //Establezca la intención de la plantilla para
            // los clics de elementos que completará cada elemento
            Intent viewIntent = new Intent(Intent.ACTION_VIEW);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);
            views.setPendingIntentTemplate(R.id.list, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetIds[i], views);
        }
    }

    // Llamado cuando se agrega el primer widget al proveedor
    @Override
    public void onEnabled(Context context) {
        //Inicie el servicio para monitorear MediaStore
        context.startService(new Intent(context, MediaService.class));
    }

    //Llamado cuando todos los widgets se han eliminado de este proveedor
    @Override
    public void onDisabled(Context context) {
        //Detenga el servicio que está monitoreando MediaStore
        context.stopService(new Intent(context, MediaService.class));
    }

    //Llamado cuando se eliminan uno o más widgets adjuntos a este proveedor
    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        //Remove the SharedPreferences we created for each widget removed
        for (int i = 0; i < appWidgetIds.length; i++) {
            context.getSharedPreferences(String.valueOf(appWidgetIds[i]), Context.MODE_PRIVATE)
                    .edit()
                    .clear()
                    .commit();
        }

    }
}
