package com.androidrecipes.appwidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;

public class SimpleAppWidget extends AppWidgetProvider {

    // Este método se llama para actualizar los widgets creados por este proveedor
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        //Start the background service to update the widget
        context.startService(new Intent(context, RandomService.class));
    }
}
