package com.androidrecipes.appwidget;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.IBinder;
import android.widget.RemoteViews;

public class RandomService extends Service {
    //Acción de transmisión cuando se completan las actualizaciones
    public static final String ACTION_RANDOM_NUMBER = "com.examples.appwidget.ACTION_RANDOM_NUMBER";

    // Datos actuales guardados como valor estático
    private static int sRandomNumber;

    public static int getRandomNumber() {
        return sRandomNumber;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Actualizar los datos del número aleatorio
        sRandomNumber = (int) (Math.random() * 100);

        //Crear la vista AppWidget
        RemoteViews views = new RemoteViews(getPackageName(), R.layout.simple_widget_layout);
        views.setTextViewText(R.id.text_number, String.valueOf(sRandomNumber));

        //Establezca una intención para el botón de
        // actualización para iniciar este servicio nuevamente
        PendingIntent refreshIntent = PendingIntent.getService(this, 0,
                new Intent(this, RandomService.class), 0);
        views.setOnClickPendingIntent(R.id.button_refresh, refreshIntent);

        //Establezca una intención para que al tocar el
        // texto del widget se abra la Actividad
        PendingIntent appIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class), 0);
        views.setOnClickPendingIntent(R.id.container, appIntent);

        //Actualiza el widget
        AppWidgetManager manager = AppWidgetManager.getInstance(this);
        ComponentName widget = new ComponentName(this, SimpleAppWidget.class);
        manager.updateAppWidget(widget, views);

        //Dispara una emisión para notificar a los oyentes
        Intent broadcast = new Intent(ACTION_RANDOM_NUMBER);
        sendBroadcast(broadcast);

        //Este servicio no debería seguir funcionando
        stopSelf();
        return START_NOT_STICKY;
    }

    //No estamos vinculados a este Servicio, por lo que este método debe
    //     * solo devuelve nulo.
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
