package com.androidrecipes.appwidget;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;

public class MediaService extends Service {

    private ContentObserver mMediaStoreObserver;

    @Override
    public void onCreate() {
        super.onCreate();
        //Cree un registro de un nuevo observador en MediaStore cuando comience este servicio
        mMediaStoreObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                //Actualice todos los widgets actualmente adjuntos a nuestro AppWidgetProvider
                AppWidgetManager manager = AppWidgetManager.getInstance(MediaService.this);
                ComponentName provider = new ComponentName(MediaService.this, ListAppWidget.class);
                int[] appWidgetIds = manager.getAppWidgetIds(provider);
                //Este método activa onDataSetChanged () en RemoteViewsService
                manager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.list);
            }
        };
        //Regístrese para imágenes y videos
        getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, mMediaStoreObserver);
        getContentResolver().registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, true, mMediaStoreObserver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Dar de baja al observador cuando el servicio se detenga
        getContentResolver().unregisterContentObserver(mMediaStoreObserver);
    }

    //No estamos vinculados a este Servicio, por lo que este método debe
    // solo devuelve nulo.
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
