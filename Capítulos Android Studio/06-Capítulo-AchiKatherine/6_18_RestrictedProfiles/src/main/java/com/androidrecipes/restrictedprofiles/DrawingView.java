package com.androidrecipes.restrictedprofiles;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DrawingView extends View {

    private Paint mFingerPaint;
    private Path mPath;

    public DrawingView(Context context) {
        super(context);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawingView(Context context, AttributeSet attrs,
                       int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        //Configurar el pincel
        mFingerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFingerPaint.setStyle(Style.STROKE);
        mFingerPaint.setStrokeCap(Cap.ROUND);
        mFingerPaint.setStrokeJoin(Join.ROUND);
        //Ancho de trazo predeterminado
        mFingerPaint.setStrokeWidth(8f);
    }

    public void setPaintColor(int color) {
        mFingerPaint.setColor(color);
    }

    public void setStrokeWidth(float width) {
        mFingerPaint.setStrokeWidth(width);
    }

    public void setCanvasColor(int color) {
        setBackgroundColor(color);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                mPath = new Path();
                //Comience en el aterrizaje
                mPath.moveTo(event.getX(), event.getY());
                //Volver a dibujar
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                //Agregar todos los puntos de contacto entre eventos
                for (int i = 0; i < event.getHistorySize(); i++) {
                    mPath.lineTo(event.getHistoricalX(i),
                            event.getHistoricalY(i));
                }
                //Volver a dibujar
                invalidate();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //Dibuja el fondo
        super.onDraw(canvas);
        //Dibuja el trazo de pintura
        if (mPath != null) {
            canvas.drawPath(mPath, mFingerPaint);
        }
    }
}
