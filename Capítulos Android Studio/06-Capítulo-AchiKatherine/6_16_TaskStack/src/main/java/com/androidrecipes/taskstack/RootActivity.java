package com.androidrecipes.taskstack;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
//permite que las aplicaciones externas lancen determinadas actividades directamente, y necesita
//implementar los patrones de navegación BACK vs UP adecuados.
public class RootActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button listButton = new Button(this);
        listButton.setText("Show Family Members");
        listButton.setOnClickListener(this);

        setContentView(listButton,
                new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public void onClick(View v) {
        //Lanzar la siguiente actividad
        Intent intent = new Intent(this, ItemsListActivity.class);
        startActivity(intent);
    }
}
