package com.androidrecipes.contacts;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;

public class ContactsActivity extends FragmentActivity {

    private static final int ROOT_ID = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout rootView = new FrameLayout(this);
        rootView.setId(ROOT_ID);

        setContentView(rootView);

        //Crea y agrega un nuevo fragmento de lista
        getSupportFragmentManager().beginTransaction()
                .add(ROOT_ID, ContactsFragment.newInstance())
                .commit();
    }

    public static class ContactsFragment extends ListFragment
            implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {

        public static ContactsFragment newInstance() {
            return new ContactsFragment();
        }

        private SimpleCursorAdapter mAdapter;

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

            // Mostrar todos los contactos en un ListView
            mAdapter = new SimpleCursorAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, null,
                    new String[]{ContactsContract.Contacts.DISPLAY_NAME},
                    new int[]{android.R.id.text1},
                    0);
            setListAdapter(mAdapter);
            // Escuche las selecciones de artículos
            getListView().setOnItemClickListener(this);

            getLoaderManager().initLoader(0, null, this);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            // Devolver todos los contactos, ordenados por nombre
            String[] projection = new String[]{
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME
            };

            return new CursorLoader(getActivity(),
                    ContactsContract.Contacts.CONTENT_URI,
                    projection, null, null,
                    ContactsContract.Contacts.DISPLAY_NAME);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mAdapter.swapCursor(null);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View v,
                                int position, long id) {
            final Cursor contacts = mAdapter.getCursor();
            if (contacts.moveToPosition(position)) {
                int selectedId = contacts.getInt(0); // _ID column
                //Recopile datos de correo electrónico de la tabla de correo electrónico
                Cursor email = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Email.DATA},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);
                // Reúna los datos del teléfono de la mesa del teléfono
                Cursor phone = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);
                // Reúna direcciones de la tabla de direcciones
                Cursor address = getActivity().getContentResolver()
                        .query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS},
                                ContactsContract.Data.CONTACT_ID
                                        + " = " + selectedId,
                                null, null);

                // Crear el mensaje de diálogo
                StringBuilder sb = new StringBuilder();
                sb.append(email.getCount() + " Emails\n");
                if (email.moveToFirst()) {
                    do {
                        sb.append("Email: " + email.getString(0));
                        sb.append('\n');
                    } while (email.moveToNext());
                    sb.append('\n');
                }
                sb.append(phone.getCount() + " Phone Numbers\n");
                if (phone.moveToFirst()) {
                    do {
                        sb.append("Phone: " + phone.getString(0));
                        sb.append('\n');
                    } while (phone.moveToNext());
                    sb.append('\n');
                }
                sb.append(address.getCount() + " Addresses\n");
                if (address.moveToFirst()) {
                    do {
                        sb.append("Address:\n"
                                + address.getString(0));
                    } while (address.moveToNext());
                    sb.append('\n');
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(contacts.getString(1)); // Display name
                builder.setMessage(sb.toString());
                builder.setPositiveButton("OK", null);
                builder.create().show();

                // Finalizar cursores temporales
                email.close();
                phone.close();
                address.close();
            }
        }
    }
}
