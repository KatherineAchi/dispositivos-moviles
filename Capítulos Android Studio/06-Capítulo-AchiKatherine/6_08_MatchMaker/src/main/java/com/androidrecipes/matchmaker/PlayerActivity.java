package com.androidrecipes.matchmaker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class PlayerActivity extends Activity {

    public static final String ACTION_PLAY = "com.examples.myplayer.PLAY";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // Inspecciona el Intent que nos lanzó
        Intent incoming = getIntent();
        //Obtenga el URI de video del campo de datos
        Uri videoUri = incoming.getData();
        //Obtenga el título opcional adicional, si existe
        String title;
        if (incoming.hasExtra(Intent.EXTRA_TITLE)) {
            title = incoming.getStringExtra(Intent.EXTRA_TITLE);
        } else {
            title = "";
        }

        //Comience a reproducir el video y mostrar el título
        Log.i("PLAYER", "Launched");
    }

}