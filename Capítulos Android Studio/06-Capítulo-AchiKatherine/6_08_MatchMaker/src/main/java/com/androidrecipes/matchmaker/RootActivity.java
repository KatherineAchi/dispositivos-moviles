package com.androidrecipes.matchmaker;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Aplicación que realiza una tarea específica y le gustaría exponer
//una interfaz para otras aplicaciones en el dispositivo para poder ejecutar su aplicación
public class RootActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button button = new Button(this);
        button.setOnClickListener(this);
        setContentView(button);
    }

    @Override
    public void onClick(View v) {
        //llama a las clases
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse("file:///android_asset/movie.m4v"), "video/h264");
        startActivity(Intent.createChooser(intent, "Play Video"));
    }
}
