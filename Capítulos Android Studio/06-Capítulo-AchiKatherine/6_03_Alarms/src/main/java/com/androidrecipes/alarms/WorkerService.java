package com.androidrecipes.alarms;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class WorkerService extends JobService {

    private static final int MSG_JOB = 1;

    //Permite que se ejecute la siguiente tarea programada
    private Handler mJobProcessor = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            JobParameters params = (JobParameters) msg.obj;
            Log.i("WorkerService", "Executing Job " + params.getJobId());
            doWork();
            jobFinished(params, false);

            return true;
        }
    });

    //Empieza el trabajo
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        Log.d("WorkerService", "Start Job " + jobParameters.getJobId());
        //Simula a lo largo de 7.5 segundos
        mJobProcessor.sendMessageDelayed(
                Message.obtain(mJobProcessor, MSG_JOB, jobParameters),
                7500
        );

        //Notifica al sistema
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Log.w("WorkerService", "Stop Job " + jobParameters.getJobId());
        //Solicitud para detener, tenemos que cancelar cualquier trabajo pendiente
        mJobProcessor.removeMessages(MSG_JOB);
        return false;
    }

    //Realiza una operación interesante, solo mostraremos la hora actual
    private void doWork() {
        Calendar now = Calendar.getInstance();
        DateFormat formatter = SimpleDateFormat.getTimeInstance();
        Toast.makeText(this, formatter.format(now.getTime()), Toast.LENGTH_SHORT).show();
    }
}
