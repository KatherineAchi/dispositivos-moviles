package com.androidrecipes.smsprovider;

import android.content.AsyncTaskLoader;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Conversations;
import android.telephony.TelephonyManager;

import java.util.List;

public class ConversationLoader extends AsyncTaskLoader<List<MessageItem>> {

    public static final String[] PROJECTION = new String[]{
            //Determinar si el mensaje es SMS o MMS
            MmsSms.TYPE_DISCRIMINATOR_COLUMN,
            //ID de artículo base
            BaseColumns._ID,
            //ID de conversación (hilo)
            Conversations.THREAD_ID,
            //Valores de fecha
            Sms.DATE,
            Sms.DATE_SENT,
            // Solo para SMS
            Sms.ADDRESS,
            Sms.BODY,
            Sms.TYPE,
            // Solo para MMS
            Mms.SUBJECT,
            Mms.MESSAGE_BOX
    };

    //ID del hilo de la conversación que estamos cargando
    private long mThreadId;
    //El número de este dispositivo
    private String mDeviceNumber;

    public ConversationLoader(Context context) {
        this(context, -1);
    }

    public ConversationLoader(Context context, long threadId) {
        super(context);
        mThreadId = threadId;
        //Obtenga el número de teléfono de este dispositivo, si está disponible
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mDeviceNumber = manager.getLine1Number();
    }

    @Override
    protected void onStartLoading() {
        //Recargar en cada solicitud de inicio
        forceLoad();
    }

    @Override
    public List<MessageItem> loadInBackground() {
        Uri uri;
        String[] projection;
        if (mThreadId < 0) {
            //Cargar todas las conversaciones
            uri = MmsSms.CONTENT_CONVERSATIONS_URI;
            projection = null;
        } else {
            //Load just the requested thread
            uri = ContentUris.withAppendedId(MmsSms.CONTENT_CONVERSATIONS_URI, mThreadId);
            projection = PROJECTION;
        }

        Cursor cursor = getContext().getContentResolver().query(
                uri,
                projection,
                null,
                null,
                null);

        return MessageItem.parseMessages(getContext(), cursor, mDeviceNumber);
    }
}
