package com.androidrecipes.smsprovider;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.Telephony.Mms;
import android.provider.Telephony.MmsSms;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Conversations;
import android.provider.Telephony.TextBasedSmsColumns;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MessageItem {
    //Identificadores de tipo de mensaje
    private static final String TYPE_SMS = "sms";
    private static final String TYPE_MMS = "mms";

    static final String[] MMS_PROJECTION = new String[]{
            //ID de artículo base
            BaseColumns._ID,
            //Tipo MIME del contenido de esta parte
            Mms.Part.CONTENT_TYPE,
            //Contenido de texto de un texto / parte sin formato
            Mms.Part.TEXT,
            //Ruta al contenido binario de una parte que no es de texto
            Mms.Part._DATA
    };

    //Id de mensaje
    public long id;
    // Identificación del hilo (conversación)
    public long thread_id;
    //Cadena de dirección del mensaje
    public String address;
    //Cuerpo del mensaje
    public String body;
    //Si este mensaje fue enviado o recibido en este dispositivo
    public boolean incoming;
    // Imagen adjunta de MMS
    public Uri attachment;

    //Construya una lista de mensajes a partir de los datos del cursor
    //consultado por el cargador
    public static List<MessageItem> parseMessages(Context context, Cursor cursor, String myNumber) {
        List<MessageItem> messages = new ArrayList<MessageItem>();
        if (!cursor.moveToFirst()) {
            return messages;
        }
        //Analizar cada mensaje según los identificadores de tipo
        do {
            String type = getMessageType(cursor);
            if (TYPE_SMS.equals(type)) {
                MessageItem item = parseSmsMessage(cursor);
                messages.add(item);
            } else if (TYPE_MMS.equals(type)) {
                MessageItem item = parseMmsMessage(context, cursor, myNumber);
                messages.add(item);
            } else {
                Log.w("TelephonyProvider", "Unknown Message Type");
            }
        } while (cursor.moveToNext());
        cursor.close();

        return messages;
    }

    //Leer el tipo de mensaje, si está presente en el cursor; de otra manera
    // inferirlo de los valores de columna presentes en el Cursor
    private static String getMessageType(Cursor cursor) {
        int typeIndex = cursor.getColumnIndex(MmsSms.TYPE_DISCRIMINATOR_COLUMN);
        if (typeIndex < 0) {
            //Escriba la columna que no está en proyección, use otro discriminador
            String cType = cursor.getString(cursor.getColumnIndex(Mms.CONTENT_TYPE));
            //Si hay un tipo de contenido, este es un mensaje MMS.
            if (cType != null) {
                return TYPE_MMS;
            } else {
                return TYPE_SMS;
            }
        } else {
            return cursor.getString(typeIndex);
        }
    }

    //Analizar un MessageItem con contenido de un mensaje SMS
    private static MessageItem parseSmsMessage(Cursor data) {
        MessageItem item = new MessageItem();
        item.id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));
        item.thread_id = data.getLong(data.getColumnIndexOrThrow(Conversations.THREAD_ID));

        item.address = data.getString(data.getColumnIndexOrThrow(Sms.ADDRESS));
        item.body = data.getString(data.getColumnIndexOrThrow(Sms.BODY));
        item.incoming = isIncomingMessage(data, true);
        return item;
    }

    // Analizar un MessageItem con contenido de un mensaje MMS
    private static MessageItem parseMmsMessage(Context context, Cursor data, String myNumber) {
        MessageItem item = new MessageItem();
        item.id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));
        item.thread_id = data.getLong(data.getColumnIndexOrThrow(Conversations.THREAD_ID));

        item.incoming = isIncomingMessage(data, false);

        long _id = data.getLong(data.getColumnIndexOrThrow(BaseColumns._ID));

        //Consultar la información de la dirección para este mensaje
        Uri addressUri = Uri.withAppendedPath(Mms.CONTENT_URI, _id + "/addr");
        Cursor addr = context.getContentResolver().query(
                addressUri,
                null,
                null,
                null,
                null);
        HashSet<String> recipients = new HashSet<String>();
        while (addr.moveToNext()) {
            String address = addr.getString(addr.getColumnIndex(Mms.Addr.ADDRESS));
            //No agregue nuestro propio número a la lista mostrada
            if (myNumber == null || !address.contains(myNumber)) {
                recipients.add(address);
            }
        }
        item.address = TextUtils.join(", ", recipients);
        addr.close();

        //Consultar todas las partes de MMS asociadas con este mensaje
        Uri messageUri = Uri.withAppendedPath(Mms.CONTENT_URI, _id + "/part");
        Cursor inner = context.getContentResolver().query(
                messageUri,
                MMS_PROJECTION,
                Mms.Part.MSG_ID + " = ?",
                new String[]{String.valueOf(data.getLong(data.getColumnIndexOrThrow(Mms._ID)))},
                null);

        while (inner.moveToNext()) {
            String contentType = inner.getString(inner.getColumnIndexOrThrow(Mms.Part.CONTENT_TYPE));
            if (contentType == null) {
                continue;
            } else if (contentType.matches("image/.*")) {
                //Encuentre cualquier parte que sea un archivo adjunto de imagen
                long partId = inner.getLong(inner.getColumnIndexOrThrow(BaseColumns._ID));
                item.attachment = Uri.withAppendedPath(Mms.CONTENT_URI, "part/" + partId);
            } else if (contentType.matches("text/.*")) {
                //Encuentra cualquier parte que sea datos de texto
                item.body = inner.getString(inner.getColumnIndexOrThrow(Mms.Part.TEXT));
            }
        }

        inner.close();
        return item;
    }

    //Validar si el mensaje es entrante o saliente por el
    // tipo información de la caja que figura en el proveedor
    private static boolean isIncomingMessage(Cursor cursor, boolean isSms) {
        int boxId;
        if (isSms) {
            boxId = cursor.getInt(cursor.getColumnIndexOrThrow(Sms.TYPE));
            // Tenga en cuenta que todos los mensajes de la tarjeta SIM tienen un boxId de cero.
            return (boxId == TextBasedSmsColumns.MESSAGE_TYPE_INBOX ||
                    boxId == TextBasedSmsColumns.MESSAGE_TYPE_ALL) ?
                    true : false;
        } else {
            boxId = cursor.getInt(cursor.getColumnIndexOrThrow(Mms.MESSAGE_BOX));
            //Tenga en cuenta que todos los mensajes de la tarjeta
            // SIM tienen un boxId de cero: Mms.MESSAGE_BOX_ALL
            return (boxId == Mms.MESSAGE_BOX_INBOX || boxId == Mms.MESSAGE_BOX_ALL) ?
                    true : false;
        }
    }
}
