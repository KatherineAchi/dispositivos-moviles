package com.androidrecipes.backgroundworker;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

public class ImageProcessor extends HandlerThread implements Handler.Callback {
    public static final int MSG_SCALE = 100;
    public static final int MSG_CROP = 101;

    private Context mContext;
    private Handler mReceiver, mCallback;

    public ImageProcessor(Context context) {
        this(context, null);
    }

    public ImageProcessor(Context context, Handler callback) {
        super("AndroidRecipesWorker");
        mCallback = callback;
        mContext = context.getApplicationContext();
    }

    @Override
    protected void onLooperPrepared() {
        mReceiver = new Handler(getLooper(), this);
    }

    @Override
    public boolean handleMessage(Message msg) {
        Bitmap source, result;
        // Recuperar argumentos del mensaje entrante
        int scale = msg.arg1;
        switch (msg.what) {
            case MSG_SCALE:
                source = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.ic_launcher);
                //Crea una nueva imagen ampliada
                result = Bitmap.createScaledBitmap(source,
                        source.getWidth() * scale, source.getHeight() * scale, true);
                break;
            case MSG_CROP:
                source = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.ic_launcher);
                int newWidth = source.getWidth() / scale;
                //Crea una nueva imagen recortada horizontalmente
                result = Bitmap.createBitmap(source,
                        (source.getWidth() - newWidth) / 2, 0,
                        newWidth, source.getHeight());
                break;
            default:
                throw new IllegalArgumentException("Unknown Worker Request");
        }

        // Devuelve la imagen al hilo principal
        if (mCallback != null) {
            mCallback.sendMessage(Message.obtain(null, 0, result));
        }
        return true;
    }

    //Agregar o quitar un controlador de devolución de llamada
    public void setCallback(Handler callback) {
        mCallback = callback;
    }


    // Escale el icono al valor especificado
    public void scaleIcon(int scale) {
        Message msg = Message.obtain(null, MSG_SCALE, scale, 0, null);
        mReceiver.sendMessage(msg);
    }

    //Recorta el ícono en el centro y escala el resultado al valor especificado
    public void cropIcon(int scale) {
        Message msg = Message.obtain(null, MSG_CROP, scale, 0, null);
        mReceiver.sendMessage(msg);
    }
}
