package com.androidrecipes.backgroundworker;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
//Necesita crear un subproceso en segundo plano de larga duración que esté esperando a que se ejecute el trabajo y que puede ser
//termina fácilmente cuando ya no se necesita.
public class WorkerActivity extends Activity implements Handler.Callback {

    private ImageProcessor mWorker;
    private Handler mResponseHandler;

    private ImageView mResultView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mResultView = (ImageView) findViewById(R.id.image_result);
        //Controlador para asignar devoluciones de llamada en segundo plano a esta actividad
        mResponseHandler = new Handler(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Iniciar un nuevo trabajador
        mWorker = new ImageProcessor(this, mResponseHandler);
        mWorker.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Terminar el trabajador
        mWorker.setCallback(null);
        mWorker.quit();
        mWorker = null;
    }

    //Método de devolución de llamada para resultados en segundo plano.
    @Override
    public boolean handleMessage(Message msg) {
        Bitmap result = (Bitmap) msg.obj;
        mResultView.setImageBitmap(result);
        return true;
    }
    
    //Métodos de acción para publicar trabajos de fondo

    public void onScaleClick(View v) {
        for (int i = 1; i < 10; i++) {
            mWorker.scaleIcon(i);
        }
    }

    public void onCropClick(View v) {
        for (int i = 1; i < 10; i++) {
            mWorker.cropIcon(i);
        }
    }
}
