package com.androidrecipes.sticky;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class ReportActivity extends Activity {
    //Método Crear
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logEvent("CREATE");
    }
    //Método Empezar
    @Override
    public void onStart() {
        super.onStart();
        logEvent("START");
    }
    //´Método Resumen
    @Override
    public void onResume() {
        super.onResume();
        logEvent("RESUME");
    }
    //Método Pausa
    @Override
    public void onPause() {
        super.onPause();
        logWarning("PAUSE");
    }

    //Método Parar
    @Override
    public void onStop() {
        super.onStop();
        logWarning("STOP");
    }

    //Método Salir
    @Override
    public void onDestroy() {
        super.onDestroy();
        logWarning("DESTROY");
    }

    //Acciona el evento
    private void logEvent(String event) {
        Intent intent = new Intent(this, OperationsManager.class);
        intent.setAction(OperationsManager.ACTION_EVENT);
        intent.putExtra(OperationsManager.EXTRA_NAME, event);

        startService(intent);
    }

    //Evento largo
    private void logWarning(String event) {
        Intent intent = new Intent(this, OperationsManager.class);
        intent.setAction(OperationsManager.ACTION_WARNING);
        intent.putExtra(OperationsManager.EXTRA_NAME, event);

        startService(intent);
    }
}