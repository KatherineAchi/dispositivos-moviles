package com.androidrecipes.preferences;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class SettingsActivity extends PreferenceActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Carga datos de preferencias desde XML
        addPreferencesFromResource(R.xml.settings);
    }

}
