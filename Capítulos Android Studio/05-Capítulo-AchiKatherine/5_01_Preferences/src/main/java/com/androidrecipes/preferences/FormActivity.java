package com.androidrecipes.preferences;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class FormActivity extends Activity implements View.OnClickListener {
//Declara objetos
    EditText email, message;
    CheckBox age;
    Button submit;

    SharedPreferences formStore;

    boolean submitSuccess = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);
    // Instancia los objetos
        email = (EditText) findViewById(R.id.email);
        message = (EditText) findViewById(R.id.message);
        age = (CheckBox) findViewById(R.id.age);

        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);

        //Recupera y crea el objeto de preferencias
        formStore = getPreferences(Activity.MODE_PRIVATE);
    }
    // Restaura los datos del formulario
    @Override
    public void onResume() {
        super.onResume();
        email.setText(formStore.getString("email", ""));
        message.setText(formStore.getString("message", ""));
        age.setChecked(formStore.getBoolean("age", false));
    }

    // Encadena las llamadas
    @Override
    public void onPause() {
        super.onPause();
        if (submitSuccess) {
            formStore.edit().clear().commit();
        } else {
            //Almacena los datos del formulario
            SharedPreferences.Editor editor = formStore.edit();
            editor.putString("email", email.getText().toString());
            editor.putString("message", message.getText().toString());
            editor.putBoolean("age", age.isChecked());
            editor.commit();
        }
    }

    @Override
    public void onClick(View v) {

        //Envia un mensaje de servicio
        submitSuccess = true;
        //Cierra
        finish();
    }
}
