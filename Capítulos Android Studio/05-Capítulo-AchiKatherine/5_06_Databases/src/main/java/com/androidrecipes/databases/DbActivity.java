package com.androidrecipes.databases;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
//Conserva datos que luego se pueden consultar o modificar
// como subconjuntos o registros individuales

public class DbActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    //Crea objetos
    EditText mText;
    Button mAdd;
    ListView mList;

    MyDbHelper mHelper;
    SQLiteDatabase mDb;
    Cursor mCursor;
    SimpleCursorAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        //Declara objetos
        mText = (EditText) findViewById(R.id.name);
        mAdd = (Button) findViewById(R.id.add);
        mAdd.setOnClickListener(this);
        mList = (ListView) findViewById(R.id.list);
        mList.setOnItemClickListener(this);

        mHelper = new MyDbHelper(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Abre las conexiones a la base de datos
        mDb = mHelper.getWritableDatabase();
        String[] columns = new String[]{"_id", MyDbHelper.COL_NAME, MyDbHelper.COL_DATE};
        mCursor = mDb.query(MyDbHelper.TABLE_NAME, columns, null, null, null, null, null, null);
        //Actualiza la lista
        String[] headers = new String[]{MyDbHelper.COL_NAME, MyDbHelper.COL_DATE};
        mAdapter = new SimpleCursorAdapter(this, android.R.layout.two_line_list_item,
                mCursor, headers, new int[]{android.R.id.text1, android.R.id.text2});
        mList.setAdapter(mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Cierra las conexiones
        mDb.close();
        mCursor.close();
    }

    @Override
    public void onClick(View v) {
        //Añade una nueva base de datos
        ContentValues cv = new ContentValues(2);
        cv.put(MyDbHelper.COL_NAME, mText.getText().toString());
        //Crear un formato de fecha SQL
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cv.put(MyDbHelper.COL_DATE, dateFormat.format(new Date())); //Insert 'now' as the date
        mDb.insert(MyDbHelper.TABLE_NAME, null, cv);
        //Actualizar la lista
        mCursor.requery();
        mAdapter.notifyDataSetChanged();
        //Borra el campo de edición
        mText.setText(null);
    }

    //Métdodo OnItenclick
    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        //Elimina elementos de la base de datos
        mCursor.moveToPosition(position);
        //Obtiene la identificación de la fila
        String rowId = mCursor.getString(0); //Column 0 of the cursor is the id
        mDb.delete(MyDbHelper.TABLE_NAME, "_id = ?", new String[]{rowId});
        //Actualiza la lista

        mCursor.requery();
        mAdapter.notifyDataSetChanged();
    }
}