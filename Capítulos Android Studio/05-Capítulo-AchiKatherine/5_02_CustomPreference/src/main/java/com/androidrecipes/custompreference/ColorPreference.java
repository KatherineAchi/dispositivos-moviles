package com.androidrecipes.custompreference;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.DialogPreference;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

public class ColorPreference extends DialogPreference {

    private static final int DEFAULT_COLOR = Color.WHITE;
//   Configuración de color actual
    private int mCurrentColor;
    //Deslizadores para configurar los componentes de color
    private SeekBar mRedLevel, mGreenLevel, mBlueLevel;

    public ColorPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //Construye un nuevo diálogo para mostrar cuando la preferencia
    @Override
    protected void onPrepareDialogBuilder(Builder builder) {
        //Crear la vista de contenido del cuadro de diálogo
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.preference_color, null);
        mRedLevel = (SeekBar) rootView.findViewById(R.id.selector_red);
        mGreenLevel = (SeekBar) rootView.findViewById(R.id.selector_green);
        mBlueLevel = (SeekBar) rootView.findViewById(R.id.selector_blue);

        mRedLevel.setProgress(Color.red(mCurrentColor));
        mGreenLevel.setProgress(Color.green(mCurrentColor));
        mBlueLevel.setProgress(Color.blue(mCurrentColor));

        //Adjuntar la vista de contenido
        builder.setView(rootView);
        super.onPrepareDialogBuilder(builder);
    }

    //Llamado cuando el diálogo se cierra
    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            //El usuario toca el botón
            int color = Color.rgb(
                    mRedLevel.getProgress(),
                    mGreenLevel.getProgress(),
                    mBlueLevel.getProgress());
            setCurrentValue(color);
        }
    }

    //Obtiene un valor predeterminado de XML
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        //Devuelve un valor INT predeterminado de XML como un color
        ColorStateList value = a.getColorStateList(index);
        if (value == null) {
            return DEFAULT_COLOR;
        }
        return value.getDefaultColor();
    }

    //Establece el valor inicial
    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        setCurrentValue(restorePersistedValue ? getPersistedInt(DEFAULT_COLOR) : (Integer) defaultValue);
    }

    //Devuelve un resumen personalizado basado en la configuración actual
    @Override
    public CharSequence getSummary() {
        //Construya el resumen con el valor del color en hexadecimal
        int color = getPersistedInt(DEFAULT_COLOR);
        String content = String.format("Current Value is 0x%02X%02X%02X",
                Color.red(color), Color.green(color), Color.blue(color));
        //Devuelve el texto de resumen coloreado por la selección
        Spannable summary = new SpannableString(content);
        summary.setSpan(new ForegroundColorSpan(color), 0, summary.length(), 0);
        return summary;
    }

    private void setCurrentValue(int value) {
        //Actualiza el último valor
        mCurrentColor = value;
        persistInt(value);
        //Notifica las preferencias
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }

}
