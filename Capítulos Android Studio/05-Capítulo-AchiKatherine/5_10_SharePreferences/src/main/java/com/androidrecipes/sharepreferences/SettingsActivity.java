package com.androidrecipes.sharepreferences;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
//Proporciona los valores de configuración que ha almacenado en SharedPreferences para
//otras aplicaciones del sistema e incluso para permitir que esas aplicaciones modifiquen esos ajustes si tienen
//permiso para hacerlo.

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Carga los valores predeterminados de preferencias en la primera ejecución
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        addPreferencesFromResource(R.xml.preferences);
    }
}
